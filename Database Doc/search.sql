/*
userid:characters+time
time:time
type:search / favorite
record: key words
...should add other object similar to post table
*/
CREATE TABLE searchHistory(
	USERID VARCHAR(128) NOT NULL,
	TIME VARCHAR(128),
	TYPE VARCHAR(64),
	RECORD TEXT
)
/*
userid:characters+time
postid:characters+time, this should be similar to post table 
statue:true or false
*/
CREATE TABLE comparisonList(
	USERID VARCHAR(128) NOT NULL,
	POSTID VARCHAR(128) NOT NULL,
	STATUE VARCHAR(32),
)