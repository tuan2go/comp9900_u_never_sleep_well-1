import smtplib
from email.mime.text import MIMEText
from email.header import Header
from flask import Flask
from flask_pymongo import PyMongo,ObjectId
app = Flask(__name__)
import csv
import numpy as np

def initMongo():
    app.config.update(
        MONGO_URI='mongodb://localhost:27017/comp9900',
        # MONGO_USERNAME='myUserAdmin',
        # MONGO_PASSWORD='abc123',
       # MONGO_TEST_URI='mongodb://localhost:27017/test'
    )
    mongo = PyMongo(app)
    return mongo

mongo = initMongo()

'''
 recipient: address of target user/XX@gmail.com
 subject_of_email: subject of email you send
 message_type: html or plain
 message: message you wanna send
'''
def sendEmail(recipient, subject_of_email, message_type, message):
    From = "unswbookDeveloper@gmail.com"
    To = [recipient] if isinstance(recipient, list) else [recipient]
    mail_pass = "unswbookroot"

    message = MIMEText(message, message_type, 'utf-8')
    message['From'] = Header("WeStay Developer", 'utf-8')
    message['To'] = Header(recipient, 'utf-8')
    message['Subject'] = Header(subject_of_email, 'utf-8')

    try:
        server = smtplib.SMTP('smtp.gmail.com:587')
        server.ehlo()
        server.starttls()
        server.login(From, mail_pass)
        server.sendmail(From, To, message.as_string())
        server.quit()
        print("Sent email successfully!")
    except smtplib.SMTPException:
        print("Error: failed to send email!")

def initPostData():
    mongo = initMongo()
    collection =  mongo.db.post
    with open('listings.csv', 'r',encoding='UTF-8') as csvfile:
        reader = csv.reader(csvfile)
        for row in reader:
            if reader.line_num == 1:
                continue
            date = "/".join(row[75].split("-"))
            code = row[43]
            if len(code)>4:
                if "NSW" in code:
                    code = code[-4:]
                else:
                    code = code[:4]
            apost={
                'source_type': 'getdata',
                'userid':'5bc490f97d11702d39b5ac9a',
                'picture':[row[17]],
                'name':row[4],
                'describe':row[7],
                'address':row[37],
                'city':row[41],
                'street':row[37],
                'postal_code':code,
                'latitude':row[48],
                'longitude':row[49],
                'post_time':row[69],
                'start_time':date,
                'end_time':"2019/12/30",
                'type':row[51],
                'beds':row[56],
                'bedrooms':row[55],
                'bathrooms':row[54],
                'price':row[60][1:],
                'booked_time':[],
                'review_num': 0,
                'reviews_id': [],
                'state':'f',
                'facilities_for_disabled':True
            }
            if( 'parking' in row[58].lower()):
                apost['parking'] = "true"
            if ('wifi' in row[58].lower()):
                apost['wifi'] = "true"
            if ('tv' in row[58].lower()):
                apost['tv'] = "true"
            if ('gym' in row[58].lower()):
                apost['gym'] = "true"
            if ('pets' in row[58].lower()):
                apost['pets'] = "true"
            if ('air_condition' in row[58].lower()):
                apost['air_condition'] = "true"
            if ('kitchen' in row[58].lower()):
                apost['kitchen'] = "true"
            if ('linens' in row[58].lower()):
                apost['linens'] = "true"
            if ('washing_machine' in row[58].lower()):
                apost['washing_machine'] = "true"
            if ('pool' in row[58].lower()):
                apost['pool'] = "true"
            if ('lift' in row[58].lower()):
                apost['lift'] = "true"
            collection.insert_one(apost)
            print(reader.line_num)
# initPostData()


# content based recommendation system
# based on 'type'(35%) 'city'(35%) 'beds'(10%) 'bedrooms'(10%) 'bathrooms'(10%)
def content_based_recommendation_system(post_id, n):
    num_of_posts = mongo.db.post.count()
    n = min(n, num_of_posts)
    result = mongo.db.post.find_one({'_id': ObjectId(post_id)}, {'type': 1, 'city': 1, 'beds': 1, 'bedrooms': 1, 'bathrooms': 1})
    room_type = result['type']
    city = result['city']
    beds = result['beds']
    bedrooms = result['bedrooms']
    bathrooms = result['bathrooms']
    matrix = [[0 for _ in range(5)] for _ in range(num_of_posts)]
    all_posts = mongo.db.post.find({}, {'type': 1, 'city': 1, 'beds': 1, 'bedrooms': 1, 'bathrooms': 1})
    index = 0
    map = {}
    for post in all_posts:
        # matrix[index][0] = 1 if post['_id'] == post_id else 0
        matrix[index][0] = 0.35 if post['type'] == room_type else 0
        matrix[index][1] = 0.35 if post['city'] == city else 0
        matrix[index][2] = 0.1 if post['beds'] == beds else 0
        matrix[index][3] = 0.1 if post['bedrooms'] == bedrooms else 0
        matrix[index][4] = 0.1 if post['bathrooms'] == bathrooms else 0
        map[post['_id']] = matrix[index]
        index += 1
    # print(matrix)
    r = []
    for m in sorted(map, key=lambda m: sum(map[m]), reverse=True)[1:n+1]:
        temp = mongo.db.post.find_one({'_id': m}, {'picture': 1, 'name': 1})
        p = []
        # p.append(m)
        p.append(str(temp['_id']))
        p.append(temp['picture'][0])
        p.append(temp['name'])
        r.append(p)
    return r


# print(content_based_recommendation_system('5bc4515c7d1170826610e5d8', 5))
def commentCheck(comment):
    from googleapiclient import discovery

    API_KEY='AIzaSyAhPtqcegCk6tAvr1WUWNegNb74osryJJU'

    # Generates API client object dynamically based on service name and version.
    service = discovery.build('commentanalyzer', 'v1alpha1', developerKey=API_KEY)

    analyze_request = {
    'comment': { 'text': comment },
    'requestedAttributes': {'TOXICITY': {}}
    }

    response = service.comments().analyze(body=analyze_request).execute()

    # import json
    # p = json.dumps(response, indent=2)
    # print(p)
    # data = json.load(p)
    return response['attributeScores']['TOXICITY']['summaryScore']['value']

