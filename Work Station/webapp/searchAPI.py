from flask import Flask
from flask import jsonify, json
from webapp.commonFunction import *
import bson,re
import datetime
from flask_pymongo import ObjectId


mongo = initMongo()


def packageResults():
    newresult = []

def areaData():
    collection = mongo.db.post
    #result = collection.distinct("postal_code")
    result = collection.aggregate([
        {"$group":{"_id":{"postal_code":"$postal_code","city":"$city"}}}
    ]);
    a = list(result)
    codes= {}
    b = []
    for item in a:
        str = item['_id']['city']
        if "NSW" in str:
            arr = str.split(" ")
            codes[arr[-1]] = " ".join(arr[:-2])
        elif item['_id']['city'] != "":
            codes[item['_id']['postal_code']] = item['_id']
    for code in sorted(codes.keys()):
        b.append(codes[code])
    return b

areaList = areaData()

def getLikeList(userid):
    history = mongo.db.searchHistory
    collection = mongo.db.post
    query = {
        "userid":userid,
        "type": {'$in': ['like']}
    }
    result = history.find(query, {'_id': 1, 'record': 1})
    result2 = []
    for item in result:
        temp = collection.find_one({"_id":ObjectId(item['record']['postid'])})
        temp['recordid'] = item['_id']
        result2.append(temp)
    return jsonifyData(result2)

def delLikeRecord(recordid,userid):
    history = mongo.db.searchHistory
    collection = mongo.db.post
    history.remove({"_id":ObjectId(recordid)})
    query ={
        "userid":userid,
        "type":{'$in':['like']}
    }
    result = history.find(query,{'_id': 1,'record':1})
    result2 = []
    for item in result:
        temp = collection.find_one({"_id":ObjectId(item['record']['postid'])})
        temp['recordid'] = item['_id']
        result2.append(temp)
    return jsonifyData(result2)


def addLikeRecord(postid,userid):
    history = mongo.db.searchHistory
    collection = mongo.db.post
    recordhistory = {}
    recordhistory['type'] = "like"
    recordhistory['userid'] = userid
    recordhistory['record'] = {
        "postid": postid
    }
    d = history.find_one(recordhistory,{"_id": 1})
    if not d:
        recordhistory['time'] = datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S")
        history.insert_one(recordhistory)
    query ={
        "type":{'$in':['like']}
    }
    result = history.find(query,{'_id': 1,'record':1})
    result2 = []
    for item in result:
        temp = collection.find_one({"_id":ObjectId(item['record']['postid'])})
        temp['recordid'] = item['_id']
        result2.append(temp)
    return jsonifyData(result2)

def search(indate,outdate,city,beds,type,bedrooms,bathrooms,price,keywords,checkboxlist,userid):
    collection = mongo.db.post
    history = mongo.db.searchHistory
    flag = 0
    record = {}
    qurey = {}
    if(indate):
        flag =1;
        qurey['start_time'] ={"$lt": indate}
        record['start_time'] =indate
    if(outdate):
        flag = 1;
        qurey['end_time'] = {"$gt": outdate}
        record['end_time'] = outdate
    if(city):
        flag = 1;
        record['city'] = city
        rexExp = re.compile('.*' + city + '.*', re.IGNORECASE)
        qurey['city'] = rexExp
    if(beds and beds !=""):
        flag = 1;
        record['beds'] = beds
        if(beds == "5+"):
            qurey['beds'] = {"$gt":"5"}
        else:
            qurey['beds'] = {'$in':[beds,""]}
    if(type and type !=""):
        flag = 1;
        qurey['type'] = type
        record['type'] = type
    if(bedrooms and bedrooms !=""):
        flag = 1;
        record['bedrooms'] = bedrooms
        if (bedrooms == "5+"):
            qurey['bedrooms'] = {"$gt": "5"}
        else:
            qurey['bedrooms'] = {'$in':[bedrooms,""]}
    if (bathrooms and bathrooms != ""):
        flag = 1;
        record['bathrooms'] = bathrooms
        if (bathrooms == "5+"):
            qurey['bathrooms'] = {"$gt": "5"}
        else:
            qurey['bathrooms'] = {'$in':[bathrooms,""]}
    if(price and price !=""):
        flag = 1;
        record['price'] = price
        if(price == "0"):
            qurey['price'] = {'$lt':"100","$gt":"0"}
        elif(price == "100"):
            qurey['price'] = {'$lt': "200", "$gt": "100"}
        elif (price == "200"):
            qurey['price'] = {'$lt': "300", "$gt": "200"}
        elif (price == "300"):
            qurey['price'] = {"$gt": "300"}
    if(keywords and keywords !=""):
        flag = 1;
        record['describe'] = keywords
        keywords = ".*".join(keywords.split(" "))
        rexExp = re.compile('.*'+keywords+'.*', re.IGNORECASE)
        qurey['$or'] = [{'describe':rexExp},{'name':rexExp},{'city':rexExp}]
        # qurey['describe'] = rexExp
    for item in checkboxlist:
        flag = 1;
        qurey[item] = "true"
    record['checkboxlist'] = checkboxlist
    qurey['state'] = "f";
    recordhistory = {
        'time': datetime.datetime.now().strftime("%Y-%m-%d %H:%M:%S"),
        'type': 'search',
        'userid': userid,
        'record': record
    }
    if flag ==1:
        result = collection.find(qurey);
        history.insert_one(recordhistory)
    else:
        result = collection.find(qurey).limit(8)

    return jsonifyData(result)

def jsonifyData(data):
    data = [serial(item) for item in data]
    return jsonify(data=data)

def serial(dct):
    for k in dct:
        if isinstance(dct[k], bson.ObjectId):
            dct[k] = str(dct[k])
    return dct


