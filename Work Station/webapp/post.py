import webapp.commonFunction as common
from flask import render_template, session, url_for,request,redirect, make_response, flash,jsonify,send_file
from webapp import app
import os
from datetime import datetime,timedelta
from flask_pymongo import ObjectId


mongo = common.initMongo()


loginPage = 'login.html'
@app.route('/post',methods = ['GET', 'POST'])
def post():
    userid = ""
    if session.get('userid'):
        userid = session['userid']
    else:
        return render_template(loginPage)
    city = request.form.get('city', '')
    street = request.form.get('Street-Address', '')
    postal_code = request.form.get('Postal-code', '')
    latitude = float(request.form.get('latitude', ''))
    longitude = float(request.form.get('longitude', ''))
    address = street+', '+city+', '+postal_code
    type = request.form.get('type', '')
    bedrooms = request.form.get('bedrooms')
    bathrooms = request.form.get('bathrooms', '')
    beds = request.form.get('beds', '')
    price = request.form.get('price', '')
    start_time = request.form.get('start_time', '')
    end_time = request.form.get('end_time', '')
    name = request.form.get('name', '')
    wifi = request.form.get('wifi', '')
    parking = request.form.get('parking', '')
    gym = request.form.get('gym', '')
    pets = request.form.get('pets', '')
    air_condition = request.form.get('air_condition', '')
    kitchen = request.form.get('kitchen', '')
    linens = request.form.get('linens', '')
    washing_machine = request.form.get('washing_machine', '')
    tv = request.form.get('tv', '')
    pool = request.form.get('pool', '')
    lift = request.form.get('lift', '')
    facilities_for_disabled = request.form.get('facilities_for_disabled', '')
    describe = request.form.get('describe', '')
    imgs = request.files.getlist('myFile[]')
    time = datetime.now()
    data = {
        "userid":userid,
    }
    x=mongo.db.post.insert_one(data)
    basepath = app.config['basedir']+'/post-picture/'+str(x.inserted_id)
    picture =[]
    #save pictures
    if not os.path.exists(basepath):
        os.mkdir(basepath)
    for img in imgs:
        print("pooooooooooooooooooooo")
        filename = img.filename
        picture.append('/image/'+str(x.inserted_id)+'/'+filename)
        path = os.path.join(basepath,filename)
        img.save(path)

    #insert to mongodb
    data = {
        "userid":userid,
        "city": city,
        "street": street,
        "postal_code": postal_code,
        "type": type,
        "bedrooms": bedrooms,
        "bathrooms": bathrooms,
        "beds": beds,
        "price": price,
        "name": name,
        "picture":picture,
        "parking": parking,
        "gym": gym,
        "pets": pets,
        "air_condition": air_condition,
        "kitchen": kitchen,
        "linens": linens,
        "washing_machine": washing_machine,
        "tv": tv,
        "pool": pool,
        "lift": lift,
        "wifi": wifi,
        "facilities_for_disabled": facilities_for_disabled,
        "describe": describe,
        "state":"f",
        "post_time": time,
        "start_time": start_time,
        "end_time": end_time,
        "booked_time":[],
        "address":address,
        "latitude":latitude,
        "longitude":longitude,
        "source_type":"input_data",
        "review_num":0,
        "reviews_id":[],
    }
    mongo.db.post.update_one({"_id":x.inserted_id},{"$set":data})
    res=mongo.db.post.find({"city":"ROSEBERRY"})
    for r in res:
        print(r.get("_id", ''))

    print("ppppppppppppppppppppppppppppppp")
    ii=str(x.inserted_id)
    ss={"code":0,
        "id":ii}
    return jsonify(ss)


@app.route('/post_detail/<post_id>',methods = ['GET', 'POST'])
def post_detail(post_id):
    #print(mongo.db.post.find({"userid": "111111"}).count())
    #post_id="5bc2c98c7d1170d16d553f00"
    print(post_id)
    post = mongo.db.post.find_one({"_id":ObjectId(post_id)})
    userid = post["userid"]
    user = mongo.db.users.find_one({"_id": ObjectId(userid)})
    reviews_id = post["reviews_id"]
    reviews=[]
    for i in reviews_id:
        review = mongo.db.reviews.find_one({"_id": ObjectId(i)})
        review["post_time"] = str(review["post_time"])
        reviews.append(review)
    print(post["type"])
    related=common.content_based_recommendation_system(post_id,4)
    print(related)
    booked_time=[]

    for j in post['booked_time']:
        k=j.split('/')
        jjjj=list(map(int, k))
        jjjj[1]=jjjj[1]-1
        if jjjj[1]==0:
            jjjj[1]=12
        booked_time.append(jjjj)
        print(jjjj)
    print(booked_time)
    book = 'false'
    print(user)
    useridd = ""
    username = ''
    if session.get('userid'):
        useridd = session['userid']
        username = session['username']
        userr = mongo.db.users.find_one({"_id": ObjectId(useridd)})
        if post_id in userr['booked_id']:
            book = 'true'
    return render_template("post_detail.html",post=post,users=user,reviews=reviews,related=related,booked_time=booked_time,book=book, username=username)


@app.route('/reviews',methods = ['GET', 'POST'])
def reviews():
    userid = ""
    if session.get('userid'):
        userid = session['userid']
    else:
        return render_template(loginPage)
    user = mongo.db.users.find_one({"_id": ObjectId(userid)})
    content = request.form.get('review_content', '')
    if common.commentCheck(content) > 0.8:
        ss = {"code": 1}
        return jsonify(ss)
    property_id = request.form.get('property_id', '')
    time = datetime.now()
    data = {
        "property_id": property_id,
        "post_time": time,
        "content": content,
        "user_id": userid,
        "user_name": user["username"],
        "user_image": user["usericon"],
    }
    x= mongo.db.reviews.insert_one(data)
    reviews_id = user["reviews_id"]
    reviews_id.insert(0,str(x.inserted_id))
    data ={
        "reviews_id":reviews_id,
    }
    mongo.db.users.update_one({"_id": user["_id"]}, {"$set": data})
    gg = mongo.db.post.find_one({"_id":ObjectId(property_id)})
    reviews_id = gg["reviews_id"]
    reviews_id.insert(0, str(x.inserted_id))
    num=gg["review_num"]+1
    data ={
        "reviews_id":reviews_id,
        "review_num":num,
    }
    mongo.db.post.update_one({"_id": gg["_id"]}, {"$set": data})
    ss = {"code": 0,
          "id":property_id}
    return jsonify(ss)


@app.route('/book',methods = ['GET', 'POST'])
def book():
    userid = ""
    if session.get('userid'):
        userid = session['userid']
    else:
        return render_template(loginPage)
    chend_time = request.form.get('chend_time', '')
    chstart_time = request.form.get('chstart_time', '')
    start = datetime.strptime(chstart_time, "%Y/%m/%d")
    end = datetime.strptime(chend_time, "%Y/%m/%d")

    book_date =[]
    for d in gen_dates(start, (end - start).days):
        str(d)
        t=d.strftime("%Y/%m/%d")
        t[:10]
        book_date.append(t)
        print(t)
    property_id = request.form.get('property_id', '')
    time = datetime.now()
    data = {
        "booked_time": book_date,
        "property_id": property_id,
        "time": time,
        "user_id": userid,
    }
    x= mongo.db.book.insert_one(data)
    gg = mongo.db.post.find_one({"_id":ObjectId(property_id)})
    print(userid)
    user = mongo.db.users.find_one({"_id": ObjectId(userid)})
    booked_id = user['booked_id']
    booked_id.insert(0,str(gg['_id']))
    data = {
        "booked_id": booked_id,
    }
    mongo.db.users.update_one({"_id": user["_id"]}, {"$set": data})
    booked_time = gg["booked_time"]
    for i in book_date:
        booked_time.insert(0, i)
    data ={
        "booked_time":booked_time,
    }
    mongo.db.post.update_one({"_id": gg["_id"]}, {"$set": data})
    ss = {"code": 0,
          "id":property_id}
    return jsonify(ss)


@app.route('/test2',methods = ['GET', 'POST'])
def test2():
    return render_template("test2.html")


def gen_dates(b_date, days):
    day = timedelta(days=1)
    for i in range(days):
        yield b_date + day*i